<?php
session_start();
include('./fonction/bdd.inc.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <title>Connexion</title>
    </head>
    <body>
        <div id="bloc_page">
	<header>
		<div id="titre_principal">
			<div id="logo">
	                        <img src="images/logo_fac.jpg" alt="Logo du jeu" />
	                        <h1>Magic Empire</h1>
			</div>
			<h2>La passion d'un jeu</h2>
                </div>
		<nav>
                    <ul>
			<li><a href="./index.php">Accueille</a></li>
			<li><a href="./classement.php">Classement</a></li>
			<li><a href="./recherche.php">Recherche</a></li>
			<li><a href="./historique.php">Historique</a></li>
                    </ul>
                </nav>
        </header>
	<div id="banniere_image" style="background: url('images/aoe.jpg') no-repeat;">
	</div>
	<section>
		<article>
			<?php
				if(!empty($_POST['deco'])){
					session_destroy();
					echo '<meta http-equiv="refresh" content="0" />';
					
				}
				if(!empty($_SESSION['connecte'])){
					
				?>	
					<form method="post" action="connexion.php">
						<fieldset>
					      		<legend>Ajouter UnitÃ©</legend> <!-- Titre du fieldset -->
							<p>
							       <label for="noms_add">Quel est le nom de l'unitÃ© ?</label>
							       <input type="text" name="nom_add" id="nom_add" />
							</p>  
							<p>
							       <label for="hp_add">Point de vie</label>
							       <input type="number" name="hp_add" id="hp_add" />
							</p>
							<p>
							       <label for="atk_add">Quel sont les degats d'attaque ?</label>
							       <input type="text" name="atk_add" id="atk_add" />
							</p> 
							<p>
							       <label for="ms_add">Quel est la vitesse ?</label>
							       <input type="number" name="ms_add" id="ms_add" />
							</p>
							<p>
							       <label for="range_add">Quel est la portÃ©e ?</label>
							       <input type="number" name="range_add" id="range_add" />
							</p>
							<p>
							       <label for="wood_add">Bois necessaire ?</label>
							       <input type="number" name="wood_add" id="wood_add" />
							</p>
							<p>
							       <label for="gold_add">Or necessaire ?</label>
							       <input type="number" name="gold_add" id="gold_add" />
							</p>
							<p>
							       <label for="food_add">Nourriture necessaire ?</label>
							       <input type="number" name="food_add" id="food_add" />
							</p>
							<p>
							       <label for="type_add">Quel est le type ?</label>
							       <input type="text" name="type_add" id="type_add" />
							</p>
							<p>
							       <label for="tc_add">Quel est le temps de construction</label>
							       <input type="number" name="tc_add" id="tc_add" />
							</p>
							<p>
								A quel faction appartiennt cette unite
								<select name="faction">
									<option value="Selesnia">Selesnia</option>
									<option value="Rakdos">Rakdos</option>
									<option value="Azorius">Azorius</option>
								</select>
							</p>
							<p>
							       <label for="bt_add">A quel batiment l'unitÃ© appartient ?</label>
							       <input type="text" name="bt_add" id="bt_add" />
							</p>
							<p>
								<input type="submit" name="maj_unite" value="Ajouter unite"/>
							</p>
						</fieldset>
						<fieldset>
					      		<legend>Maj UnitÃ©</legend> <!-- Titre du fieldset -->
							<p>
								On change la valeur de
								<select name="type_maj">
									<option value="HP_u">PV</option>
									<option value="ATK_u">ATK</option>
									<option value="MS_u">MS</option>
 									<option value="RANGE_u">Porte</option>
									<option value="GOLD_u">OR</option>
									<option value="WOOD_u">BOIS</option>
									<option value="FOOD_u">NOURRITURE</option>
									<option value="TC_u">TC</option>
								</select>
							        <label for="name_maj">de l'unitÃ©</label>
							   	<input type="text" name="name_maj" id="name_maj" />
								<label for="val_maj">la valeur sera de </label>
							        <input type="number" name="val_maj" id="val_maj" />
							</p>
							<p>
								<input type="submit" name="maj_unite" value="maj unite"/>
							</p>
						</fieldset>
						<fieldset>
				      		<legend>Supprimer UnitÃ©</legend> <!-- Titre du fieldset --> 
							<p>
							       <label for="supp_unit">Quel est le nom de l'unite a supprimer ?</label>
							       <input type="text" name="supp_unit" id="supp_unit" />
							</p>
							<p>
								<input type="submit" name="supp_unite" value="supp unite"/>
							</p>
						</fieldset>
						
					</form>
					<form method="post" action="connexion.php">
						<fieldset>
					      		<legend>Supprimer Compte</legend> <!-- Titre du fieldset --> 
							<p>
							       <label for="supp_compt">Pseudo du joueur a supprimer</label>
							       <input type="text" name="supp_compt" id="supp_compt" />
							</p>
							<p>
								<input type="submit" name="supp_compte" value="supp_compte"/>
							</p>
						</fieldset>

					</form>
					<form method="post" action="connexion.php">
						<p>
							<input type="submit" name="deco" value="deconnexion"/>
						</p>
						<p>
							<input type="submit" name="AFF" value="Afficher UnitÃ©"/>
						</p>
					</form>
				<?php
					if(!empty($_POST['nom_add'])&&!empty($_POST['hp_add'])&&!empty($_POST['atk_add'])&&!empty($_POST['ms_add'])&&!empty($_POST['range_add'])&&!empty($_POST['wood_add'])&&!empty($_POST['gold_add'])&&!empty($_POST['food_add'])&&!empty($_POST['type_add'])&&!empty($_POST['bt_add']) &&!empty($_POST['tc_add'])&&!empty($_POST['faction'])){
						$valor= array( $_POST['hp_add'] ,$_POST['atk_add'] ,$_POST['ms_add'] ,$_POST['range_add'] ,$_POST['wood_add'] ,$_POST['gold_add'] ,$_POST['food_add'] ,$_POST['tc_add'] ,"'".$_POST['bt_add']."'","'".$_POST['faction']."'","'".$_POST['nom_add']."'" ,"'".$_POST['type_add']."'");
						ajtUnite($valor);
						echo "<script>alert('UnitÃ© ".$_POST['nom_add']." ajoutÃ©');</script>";
						echo '<meta http-equiv="refresh" content="0" />';
					}
					if(!empty($_POST['type_maj'])&&!empty($_POST['name_maj'])&&!empty($_POST['val_maj'])){
						$_POST['type_maj']=failXSS($_POST['type_maj']);
						$_POST['name_maj']=failXSS($_POST['name_maj']);
						$_POST['val_maj']=failXSS($_POST['val_maj']);
						majUnite("'".$_POST['name_maj']."'",$_POST['type_maj'],$_POST['val_maj']);
						echo "<script>alert('UnitÃ© ".$_POST['name_maj']." mise a jour');</script>";
						echo '<meta http-equiv="refresh" content="0" />';
					}
					if(!empty($_POST['supp_unit'])){
						$_POST['supp_unit']=failXSS($_POST['supp_unit']);
						suppUnite("'".$_POST['supp_unit']."'");
						echo "<script>alert('UnitÃ© ".$_POST['supp_unit']."');</script>";
						echo '<meta http-equiv="refresh" content="0" />';

					}
					if(!empty($_POST['supp_compt'])){
						$_POST['supp_compt']=failXSS($_POST['supp_compt']);
						suppJoueur("'".$_POST['supp_compt']."'");
						echo "<script>alert('Joueur ".$_POST['supp_compt']." supprimÃ©');</script>";
						echo '<meta http-equiv="refresh" content="0" />';											
					}
					if(!empty($_POST['AFF'])){
						traitement(afficheUnite());
					}
				}else{
			?>
			<form method="post" action="connexion.php">
 				<fieldset>
				       <legend>Creer un compte</legend> <!-- Titre du fieldset --> 
				<p>
				       <label for="email">Quel est email ?</label>
				       <input type="email" name="email_creer" id="email_creer" />
				</p>
				<p>
				       <label for="pseudo">Quel est votre pseudo ?</label>
				       <input type="text" name="pseudo_creer" id="pseudo_creer" />
				</p> 
				<p>
				       <label for="mdp">Quel est votre mot de passe ?</label>
				       <input type="password" name="mdp_creer" id="mdp_creer" />
				</p>
				   <input type="submit" value="envoyer"/>
				   </fieldset>
   				   <fieldset>
       					<legend>Connectez-vous</legend> <!-- Titre du fieldset -->
 						<p>
						       <label for="email">Quel est votre email ?</label>
						       <input type="email" name="email_co" id="email_co" />
						 </p>
						 <p>
						       <label for="mdp">Quel est votre mot de passe ?</label>
						       <input type="password" name="mdp_co" id="mdp_co" />
      						</p>
					<input type="submit" value="envoyer"/>
				  </fieldset>
			</form>
			<?php
			}
				if(!empty($_POST['email_co'])||!empty($_POST['mdp_co'])){
					$_POST['email_co']=failXSS($_POST['email_co']);
					$_POST['mdp_co']=failXSS($_POST['mdp_co']);
					
					$bool = verifCompte("'".$_POST['email_co']."'","'".$_POST['mdp_co']."'");
					if($bool==TRUE){
						$_SESSION['connecte']=TRUE;
						echo '<meta http-equiv="refresh" content="0" />';
					}else{	
						echo "<script>alert('Erreur : pas de compte ou pas de droit admin');</script>";
					}
				}
				if(!empty($_POST['email_creer'])||!empty($_POST['mdp_creer'])||!empty($_POST['pseudo_creer'])){
					if(!existCompte("'".$_POST['email_creer']."'") && !existPseudo("'".$_POST['pseudo_creer']."'")){					
					$_POST['email_creer']=failXSS($_POST['email_creer']);
					$_POST['mdp_creer']=failXSS($_POST['mdp_creer']);
					ajtCompte("'".$_POST['email_creer']."'","'".$_POST['mdp_creer']."'");
					ajtJoueur("'".$_POST['pseudo_creer']."'","'".$_POST['email_creer']."'");
					echo "<script>alert('Valide : compte creÃ©');</script>";
					}
					else{
						echo "<script>alert('Erreur : compte ou pseudo existant');</script>";
					}
				}	
	
				


			?>
		</article>
	</section>
        </div>
    </body>
</html>
<?php
?>


<?php


?>
