<?php
include('./fonction/bdd.inc.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <title>Recherche</title>
    </head>
    <body>
        <div id="bloc_page">
	<header>
		<div id="titre_principal">
			<div id="logo">
	                        <img src="images/jeu_logo.png" alt="Logo du jeu" />
	                        <h1>Magic Empire</h1>
			</div>
			<h2>La passion d'un jeu</h2>
                </div>
		<nav>
                    <ul>
                        <li><a href="./index.php">Accueille</a></li>
			<li><a href="./classement.php">Classement</a></li>
			<li><a href="./connexion.php">Connexion</a></li>
			<li><a href="./historique.php">Historique</a></li>
                    </ul>
                </nav>
        </header>
	<div id="banniere_image" style="background: url('images/aoe.jpg') no-repeat;">
	</div>
	<section>
		<article>
			<form method="post" action="recherche.php">
				   <fieldset>
				       <legend>Pseudo recherchÃ©</legend>
				       <label for="nom">Quel est le pseudo</label>
				       <input type="text" name="pseudo" id="pseudo" />
				   </fieldset>
			</form>
			<?php
				if(!empty($_POST['pseudo'])){
					$_POST['pseudo']=htmlspecialchars($_POST['pseudo']);
					$supp = array("'",'"');
					$_POST['pseudo']=str_replace($supp,"_",$_POST['pseudo']);
					echo "<div class='joueur'>";
					echo "\t"."<h3>"."Resultat de ".$_POST['pseudo']."</h3>\n";
					$rep = rechJoueur("'".$_POST['pseudo']."'");
					if(!empty($rep)){
						traitement($rep);
					}
					echo "</div>";
					echo "<div class='joueur'>";
					echo "\t"."<h3>"."Parties jouÃ©s de ".$_POST['pseudo']."</h3>\n";
					$rep = recPartiJoueur("'".$_POST['pseudo']."'");
					if(!empty($rep)){
					traitement($rep);
					}
					echo "</div>";
					echo "<div class='joueur'>";
					echo "\t"."<h3>"."Parties gagnÃ©s de ".$_POST['pseudo']."</h3>\n";
					$rep = recPartiWinner("'".$_POST['pseudo']."'");
					if(!empty($rep)){
					traitement($rep);
					}
					echo "</div>";	
					
				}
			?>
		</article>
	</section>
        </div>
    </body>
</html>
<?php
?>


<?php


?>
