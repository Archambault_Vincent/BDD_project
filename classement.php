<?php
include('./fonction/bdd.inc.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <title>Classement</title>
    </head>
    <body>
        <div id="bloc_page">
	<header>
		<div id="titre_principal">
			<div id="logo">
	                        <img src="images/logo_fac.jpg" alt="Logo du jeu" />
	                        <h1>Magic Empire</h1>
			</div>
			<h2>La passion d'un jeu</h2>
                </div>
		<nav>
                    <ul>
			<li><a href="./index.php">Accueille</a></li>
			<li><a href="./recherche.php">Recherche</a></li>
			<li><a href="./connexion.php">Connexion</a></li>
			<li><a href="./historique.php">Historique</a></li>
                    </ul>
                </nav>
        </header>
	<div id="banniere_image" style="background: url('images/aoe.jpg') no-repeat;">
	</div>
	<section>
		<article>
			<?php
			$rep = classement();
			traitement($rep);
			?>
		</article>
	</section>

        </div>
    </body>
</html>
<?php
?>
