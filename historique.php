<?php
include('./fonction/bdd.inc.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <title>Historique</title>
    </head>
    <body>
        <div id="bloc_page">
	<header>
		<div id="titre_principal">
			<div id="logo">
	                        <img src="images/logo_fac.jpg" alt="Logo du jeu" />
	                        <h1>Magic Empire</h1>
			</div>
			<h2>La passion d'un jeu</h2>
                </div>
		<nav>
                    <ul>
			<li><a href="./index.php">Accueille</a></li>
			<li><a href="./classement.php">Classement</a></li>
			<li><a href="./recherche.php">Recherche</a></li>
			<li><a href="./connexion.php">Connexion</a></li>
                    </ul>
                </nav>
        </header>
	<div id="banniere_image" style="background: url('images/aoe.jpg') no-repeat;">
	</div>
	<section>
		<article>
			<form method="post" action="historique.php">
			   <fieldset>
			       <legend>Periode recherchÃ©</legend>
			       <label for="periode">Afficher les </label>
			       <select  name="borne" id="borne">
					<option value="<"> parties jouÃ©s avant le </option>
					<option value="="> parties jouÃ©s le </option>
					<option value=">"> parties jouÃ©s aprÃ©s le </option>
				</select>
			       <input type="date" max="2018-01-01" min="2017-09-01" name="periode" id="periode">
			       <input type="submit" value="rechercher"/>
			   </fieldset>
			</form>
			<?php
				if(!empty($_POST['periode'])&&$_POST['borne']){
					$_POST['periode']=htmlspecialchars($_POST['periode']);
					$supp = array("'",'"');
					$_POST['periode']=str_replace($supp,"_",$_POST['periode']);
					echo "<div class='historique'>";
					echo "\t"."<h3>"."Historique de la periode ".$_POST['borne']." ".$_POST['periode']."</h3>\n";
					$rep = rechPartiDate("'".$_POST['periode']."'",$_POST['borne']);
					if(!empty($rep)){
						traitement($rep);
					}
				}
			?>
		</article>
	</section>
	<footer>
              
            </footer>
        </div>
    </body>
</html>
<?php
?>
</html>
